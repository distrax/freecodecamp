let averageIt = (oldList) => {
  let temp = [];
  let newList = [];
  let newerList = [];
  oldList.map(function (elem) {
    temp.push({ "director": elem.Director, "rating": parseFloat(elem.imdbRating) });
  })

  newList = temp.filter(function (elem) {
    if (elem.director == "Christopher Nolan") {
      return true;
    }
  })
  newerList = newList.reduce(function (accumulator, currentValue) {
  return accumulator + currentValue.rating;
}, 0) / newList.length;
return newerList;
}
var averageRating = averageIt(watchList);

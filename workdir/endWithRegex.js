function confirmEnding(str, target) {
  let regExp = new RegExp(target + '$', "i");
  return regExp.test(str);
}

confirmEnding("Bastian", "n");

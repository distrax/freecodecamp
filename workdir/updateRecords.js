/*
 * This is a JavaScript Scratchpad.
 *
 * Enter some JavaScript, then Right Click or choose from the Execute Menu:
 * 1. Run to evaluate the selected text (Ctrl+R),
 * 2. Inspect to bring up an Object Inspector on the result (Ctrl+I), or,
 * 3. Display to insert the result in a comment after the selection. (Ctrl+L)
 */
var collection = {
    "2548": {
      "album": "Slippery When Wet",
      "artist": "Bon Jovi",
      "tracks": [ 
        "Let It Rock", 
        "You Give Love a Bad Name" 
      ]
    },
    "2468": {
      "album": "1999",
      "artist": "Prince",
      "tracks": [ 
        "1999", 
        "Little Red Corvette" 
      ]
    },
    "1245": {
      "artist": "Robert Palmer",
      "tracks": [ ]
    },
    "5439": {
      "album": "ABBA Gold"
    }
};
// Keep a copy of the collection for tests
var collectionCopy = JSON.parse(JSON.stringify(collection));

// Only change code below this line
function updateRecords(id, prop, value) {
  if(prop=="tracks"){
    if(collection[id].hasOwnProperty('tracks')){
      if(value==""){
          delete collection[id].tracks;
      }else{
      collection[id].tracks.push(value);
    }}else{
      collection[id].tracks = [];
      collection[id].tracks.push(value);
    }}else if(prop=="artist"){
    if(value == ""){
      delete collection[id].artist;
    }else{
    collection[id].artist = value;}
  }else{
    if(value==""){
      delete collection[id].album;
    }else{
    collection[id].album = value;
  }}
  
  return collection;
}
// Alter values below to test your code
updateRecords("5439", "artist", "ABBA");

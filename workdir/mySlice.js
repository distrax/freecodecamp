function sliceArray(anim, beginSlice, endSlice) {
if(endSlice>anim.length || endSlice == null){
  endSlice = anim.length;
}
if(beginSlice<0 || beginSlice == null){
  beginSlice=0;
}
let newArr = [];
while(beginSlice<endSlice){
  newArr.push(anim[beginSlice]);
  beginSlice++;
}
return newArr;
}
/*
function sliceArray(anim, beginSlice, endSlice){
  return anim.slice(beginSlice, endSlice);
}
*/
var inputAnim = ["Cat", "Dog", "Tiger", "Zebra", "Ant"];
sliceArray(inputAnim, 1, 3);

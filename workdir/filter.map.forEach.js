const realNumberArray = [4, 5.6, -9.8, 3.14, 42, 6, 8.34];
const squareList = (arr) => {
  "use strict";
  // change code below this line
  
  const squaredIntegers = [];
  arr.filter(nums => {
    if(Number.isInteger(nums)){
      if(nums>0){
        squaredIntegers.push(nums * nums);
      }
    }
  })
  // change code above this line
  return squaredIntegers;
};
// test your code
const squaredIntegers = squareList(realNumberArray);
console.log(squaredIntegers);
//or//
//
const sqInt = [];
arr.forEach(function(nums){
	if(Number.isInteger(nums)){
		if(nums>0){
			sqInt.push(nums*nums);
		}}});

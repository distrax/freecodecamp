
    function truthCheck(collection, pre) {
      // Is everyone being true?
      if(!collection.every(x => x.hasOwnProperty(pre))){
        return false;
      }
      collection.forEach(function(elem){
        console.log(elem);
        if(elem[pre]!==true){
          return false;
        }
      })
      return true;
    }
    
    
    truthCheck([{"user": "Tinky-Winky", "sex": "male"}, {"user": "Dipsy", "sex": "male"}, {"user": "Laa-Laa", "sex": "female"}, {"user": "Po", "sex": "female"}], "sex");
  
//Checking for every letter in second string of an array existing in first string.
function mutation(arr) {
  let letters = arr[1].toLowerCase().split('');
  for(let i = 0; i < letters.length; i++){
    if(arr[0].toLowerCase().indexOf(letters[i])==-1){
      return false;
    }
  }
  return true;
}


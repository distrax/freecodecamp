Array.prototype.myFilter = function (callback) {
  var newArray = [];
  this.forEach(function (x) { if (callback(x) == true) { newArray.push(x); } })
  return newArray;
};

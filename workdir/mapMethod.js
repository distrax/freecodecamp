Array.prototype.myMap = function (callback) {
  var newArray = [];
  this.forEach(function (x) {
    newArray.push(callback(x))
  })
  return newArray;
};
/*
 * Function mirroring the .map method in the array prototype. 
 */


//A constructor function to easily create similarly built objects

function House(numBedrooms) {
  this.numBedrooms = numBedrooms,
  House.prototype.common = 'thing' //The prototype notation creates a single variable 'common' instead of a seperate 'common' variable for every instance of House. 
}


let myHouse = new House(22); //Creates a new House like object with 22 bedrooms


myHouse instanceof House; //Returns true because myHouse is an instance of House

//Pushing property names into an array by checking that they exist

let ownProps = [];

for(let prop in myHouse) {
  if(myHouse.hasOwnProperty(prop)){
    ownProps.push(prop); //The 'common' prototype property will not get pushed to the array. It is not an 'own' property. solve with "else {ownProps.push(prop)}" due to hasOwnProperty returning false on prototype 'common'.
  }
}
//New constructor and easy way to add prototype properties. 
function Dog(name) {
  this.name = name; 
}

Dog.prototype = {
  constructor: Dog, //Must be defined. Manually setting prototypes deletes the constructor. 
  numLegs: 4, 
  eat: function(){cosole.log('Dog Eating. Could be a function to remove some amount from a food supply object')},
  describe: function(){console.log(`My name is ${this.name}.`}
};

let myDog = new Dog("Tyrus");

Dog.prototype.isPropertyOf(myDog); //returns true

/* NOTES--
 * The hasOwnProperty method is defined in Object.prototype, which can be accessed by Dog.prototype, which can then be accessed by myDog. This is an example of the prototype chain.

In this prototype chain, Dog is the supertype for myDog, while myDog is the subtype. Object is a supertype for both Dog and myDog.

Object is a supertype for all objects in JavaScript. Therefore, any object can use the hasOwnProperty method.
*/

//Lastly to create closure(not allow a property to be changed) create a block level variable inside an object along with a separate function to return it. 

function Bird() {
  let weight = 15; //Calling Bird.weight would be undefined in the global scope.
  this.getWeight = function() {
    return weight;
  };
}

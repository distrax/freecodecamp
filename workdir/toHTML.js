//Convert specific character to their HTML representations.



function convertHTML(str) {
 let newString = str.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;').replace(/'/g, '&apos;');
 console.log(newString);
  return newString;
}

convertHTML("Dolce' & \"Gabbana>"); // Returns 'Dolce&apos; &amp; &quot;Gabbana&gt;'

/* Interesting .map method solution
function convertHTML(str) {
  // Use Object Lookup to declare as many HTML entities as needed.
  htmlEntities={
    '&':'&amp;',
    '<':'&lt;',
    '>':'&gt;',
    '\"':'&quot;',
    '\'':'&apos;'
  };
  //Use map function to return a filtered str with all entities changed automatically.
  return str.split('').map(function(entity){
    return htmlEntities[entity] || entity;
  }).join('');
}
*/

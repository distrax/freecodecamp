function rot13(str) { // LBH QVQ VG!
  let newString = '';
  for(let i = 0; i<str.length; i++){
      if(/[A-Z]/.test(str[i])){
          if(str.charCodeAt(i)>77){
          newString += String.fromCharCode(str.charCodeAt(i) - 13);
          }else{
            newString += String.fromCharCode(str.charCodeAt(i) + 13);
          }
      }
      else{
          newString += str[i];
      }
  }
  return newString;
}

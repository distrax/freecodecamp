Freecodecamp project 3 for Responsive Web Design Certification: Product
Landing Page. 
This is a sample product landing page using a fictional product and company.
Some parts are created with humor in mind and the logo at the top of the page
is custom. In the HTML comments are more ideas in regards to what could have
been changed or added were the product a real product. 

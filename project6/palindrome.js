/* //Original solution
function palindrome(wrd) {
let str = wrd.toLowerCase();
str = str.replace(/[-\|&;:\$.%@"_<>\(\)\+ ,]/g, "");
let arr = [];
let reverse = [];
arr = str.split('');
for(let i = arr.length - 1; i > -1; i--){
reverse.push(arr[i]);
}
let rstr = reverse.join('');
if(rstr == str){
  return true//`${wrd} is a palindrome`;
}
  return false//`${wrd} is not a palindrome`;
}
*/

//or after a couple days of studying
//shorter


function palindrome(wrd) {
let str = wrd.toLowerCase().replace(/[-\|&;:\$.%@"_<>\(\)\+ ,]/g, "");
if(str==str.split('').reverse().join('')){
  return true
}
  return false
}

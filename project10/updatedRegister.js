//As ability improves I can write better versions of this project here..
//Find out why copies of cid are mutated when cid is. 'let startingValue = cid.slice(0);' or 'let startingValue = [...cid];'  are both still mutated when cid is. Maybe create a copy all *100 and only mutate the copy...
//Switch to defaulting to *100, cents, and only decimal when displaying.
//
function checkCashRegister(price, cash, cid) {
  let change = [];
  const currency = {
      'ONE HUNDRED' : 100,
      'TWENTY' : 20,
      'TEN' : 10,
      'FIVE' : 5, 
      'ONE' : 1, 
      'QUARTER' : .25,
      'DIME' : .10,
      'NICKEL' : .05,
      'PENNY' : .01
  }

  let owed = (cash*100 - price*100)/100;

  function cidTotal () {
    let currentTotal = 0;
    cid.forEach(function(elem){
      currentTotal+=elem[1]*100;
    })
    return currentTotal/100;
  }

  if(cidTotal()<owed){
    return {status: "INSUFFICIENT_FUNDS", change: []};
  }
  if(cidTotal()==owed){
    return {status: 'CLOSED', 'change': cid};
  }
  for(let arr = cid.length-1; arr > -1 ; arr--){
  	let pusher=0;
    while(cid[arr][1]>0&&currency[cid[arr][0]]<=owed){
      cid[arr][1]-=currency[cid[arr][0]];
      owed*=100;
      owed=Math.round(owed-currency[cid[arr][0]]*100);
      owed/=100;
      pusher+=currency[cid[arr][0]]*100;
    }
    if(pusher>0){
      change.push([cid[arr][0], pusher/100]);
    }
}
if(owed>=.01){
  return {status: "INSUFFICIENT_FUNDS", change: []};
}
  return {status: 'OPEN', 'change': change};
}

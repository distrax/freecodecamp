/*Write separate math functions to deal with all math and include precision steps*/
function checkCashRegister(price, cash, cid) {
  let change = [];
  // Here is your change, ma'am.
  const currency = {
      'ONE HUNDRED' : 100,
      'TWENTY' : 20,
      'TEN' : 10,
      'FIVE' : 5, 
      'ONE' : 1, 
      'QUARTER' : .25,
      'DIME' : .10,
      'NICKEL' : .05,
      'PENNY' : .01
  }

  let owed = (cash*100 - price*100)/100;
  let total = 0;
  
  function drawerTotal () {
    let temp2 = 0;
  cid.forEach(function(elem){
    let temp = elem[1]*100;
    temp2+=temp;
})
total = temp2/100;
  }
  drawerTotal();

  if(total<owed){
    return {status: "INSUFFICIENT_FUNDS", change: []};
  }
  if(total==owed){
    return {status: 'CLOSED', 'change': cid};
  }
  let pusher = 0;
  for(let arr = cid.length-1; arr > -1 ; arr--){
    pusher=0;
    while(cid[arr][1]>0&&currency[cid[arr][0]]<=owed){
      cid[arr][1]-=currency[cid[arr][0]];
      owed*=100;
      owed=Math.round(owed-currency[cid[arr][0]]*100);
      owed/=100;
      pusher+=currency[cid[arr][0]]*100;
    }
    if(pusher>0){
      change.push([cid[arr][0], pusher/100]);
    }
}
if(owed>=.01){
  return {status: "INSUFFICIENT_FUNDS", change: []};
}
  return {status: 'OPEN', 'change': change};
}




checkCashRegister(19.5, 20, [["PENNY", 1.01], ["NICKEL", 2.05], ["DIME", 3.1], ["QUARTER", 4.25], ["ONE", 90], ["FIVE", 55], ["TEN", 20], ["TWENTY", 60], ["ONE HUNDRED", 100]]);
